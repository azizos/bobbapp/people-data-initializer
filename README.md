# people-data-init

This project builds a Docker image that is used to import data (structured using .sql files) into a PostqreSQL database.
In order to use the image, you'll need to pass credentials of the target database by injecting the following environment variables into the container:


| Environment variable |
| -------------------- |
| POSTGRES_HOST        |
| POSTGRES_DB          |
| POSTGRES_USER        |
| POSTGRES_PASSWORD    | 

The pipeline in this project builds images and pushes to Google Container Registry. Please see the steps in `.gitlab-ci.yaml`.

# Example usage

At the moment, we're using this tool as a init-container for [people-service](https://gitlab.com/azizos/bobbapp/people-service). The `people-service` includes a  Docker-compose file that demonstrates the usage of this specific tool.

_Note: this project is being pulled in `people-service` (as a submodule) and built by docker-compose for local development. However, the CD pipeline of `people-service` uses Kubernetes to pull the already built image of this tool (to run the associated init-container).

# To improve

This project uses contents of `data` directory (assuming it only contains `.sql` files) to import data to databases. But it's not the good practice. In order to make this image/tool more usable, you'd want to keep your init/migration data in a separate repository and clone the data from there whenever running this tool. 

The current approach is mainly implemented for demo purposes. 